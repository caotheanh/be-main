import { IsNotEmpty } from 'class-validator';

export class UserInfoDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  phone: string;

  @IsNotEmpty()
  address: string;

  id?: number;
}

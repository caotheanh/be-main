import { IsNotEmpty } from 'class-validator';

export class OrdersDto {
  @IsNotEmpty()
  id_product: string;

  @IsNotEmpty()
  id_user: number;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  phone: string;

  @IsNotEmpty()
  total_number: number;

  @IsNotEmpty()
  price_ship: number;

  @IsNotEmpty()
  total_price: number;

  @IsNotEmpty()
  status: number;

  @IsNotEmpty()
  payment_type: number;

  @IsNotEmpty()
  company_id: number;

  note?: string;

  id?: number;
}

import { IsNotEmpty } from 'class-validator';

export class ProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  details: string;

  @IsNotEmpty()
  more: string;

  images?: string;

  id?: number;
}

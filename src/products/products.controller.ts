import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ProductDto } from 'src/dto/products.dto';
import { DeleteResult } from 'typeorm';
import { Products } from './product.entity';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private productServices: ProductsService) {}

  @Get()
  get(): Promise<[Products[], number]> {
    return this.productServices.listProduct();
  }

  @Get('id')
  findOne(@Param() data: any): Promise<Products> {
    const { id } = data;
    return this.productServices.listProductById(id);
  }

  @Delete()
  delete(@Body() data: any): Promise<DeleteResult> {
    const { id } = data;
    return this.productServices.deleteProduct(id);
  }

  @Post()
  @UseInterceptors(FileInterceptor('photo', { dest: './uploads' }))
  async insert(@Body() @UploadedFile() file, @Body() data: ProductDto) {
    try {
      const typeFile = ['image/png', 'image/jpg', 'image/jpeg'];
      const isImages = typeFile.filter((el) => file.mimetype === el);
      if (isImages.length !== 0) {
        return await this.productServices.addProduct(file.filename, data);
      }
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductDto } from 'src/dto/products.dto';
import { ResponseCommon } from 'src/response/responseCommon';
import { DeleteResult, Repository } from 'typeorm';
import { Products } from './product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Products)
    private readonly productRepository: Repository<Products>,
  ) {}

  sendMessagesAndStatus(messages: string, code: number) {
    return {
      messages,
      code,
    };
  }

  /**
   *
   * @returns array products and count product
   */

  async listProduct(): Promise<[Products[], number]> {
    try {
      return await this.productRepository.findAndCount();
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id product
   * @returns product
   */

  async listProductById(id: number): Promise<Products> {
    try {
      return await this.productRepository.findOne({ id });
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id product
   * @returns
   */

  async deleteProduct(id: number): Promise<DeleteResult> {
    try {
      return await this.productRepository.delete(id);
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param images name images save database
   * @param data
   * @returns code and messages
   */

  async addProduct(images: string, data: ProductDto): Promise<ResponseCommon> {
    try {
      const { name, price, total, details, more } = data;
      const dataInsert = {
        name,
        price,
        total,
        details,
        more,
        images,
      };
      const isInsertData = await this.productRepository.create(dataInsert);
      const result = await this.productRepository.save(isInsertData);
      return result
        ? this.sendMessagesAndStatus('Insert product success', 200)
        : this.sendMessagesAndStatus('Insert product faild', 400);
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }
}

export interface ResponseCommon {
  messages: string;
  code?: number;
  token?: string;
}

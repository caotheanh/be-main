import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from './coreModule';
import { authMiddleware } from './middleware/authMiddleware';
import { UserInfoController } from './user-info/user-info.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    CoreModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../..', '/be-main/uploads'),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(authMiddleware).forRoutes(UserInfoController);
  }
}

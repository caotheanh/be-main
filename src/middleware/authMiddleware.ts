import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

export async function authMiddleware(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  try {
    const token = req.body.token || req.query.token || req.headers['token'];
    const TOKEN_SERVER = 'HS256';
    const decode = await jwt.verify(token, TOKEN_SERVER);
    if (decode) {
      next();
    } else {
      return res.json({
        messages: 'authorization',
      });
    }
  } catch (error) {
    return res.json({
      messages: 'authorization',
    });
  }
}

import { Module } from '@nestjs/common';
import { CompanyModule } from './company/company.module';
import { OrdersModule } from './orders/orders.module';
import { ProductsModule } from './products/products.module';
import { UserInfoModule } from './user-info/user-info.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    UserModule,
    UserInfoModule,
    ProductsModule,
    CompanyModule,
    OrdersModule,
  ],
  exports: [
    UserModule,
    UserInfoModule,
    ProductsModule,
    CompanyModule,
    OrdersModule,
  ],
})
export class CoreModule {}

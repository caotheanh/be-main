import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserInfoDto } from 'src/dto/user-info.dto';
import { ResponseCommon } from 'src/response/responseCommon';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { UserInfo } from './user-info.entity';

@Injectable()
export class UserInfoService {
  constructor(
    @InjectRepository(UserInfo)
    private readonly userInfoRepository: Repository<UserInfo>,
    private readonly userServices: UserService,
  ) {}

  /**
   *
   * @param token token by client
   * @returns user info
   */

  async getUserInfo(token: string): Promise<UserInfo[]> {
    try {
      const id_user = await this.userServices.getIdUserByToken(token);
      return await this.userInfoRepository.find({
        where: {
          id_user,
        },
        relations: ['email'],
      });
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data data send by client name, phone ,address
   * @param token token send by headers
   * @returns messages and code
   */

  async addUserInfo(data: UserInfoDto, token: string): Promise<ResponseCommon> {
    try {
      const { address, name, phone } = data;
      const id_user = await this.userServices.getIdUserByToken(token);
      const dataInsert = { name, address, phone, id_user };
      const isDataInsert = this.userInfoRepository.create(dataInsert);
      const result = await this.userInfoRepository.save(isDataInsert);
      return result
        ? { messages: 'Register user infomation success!', code: 200 }
        : { messages: 'Register user infomation failed!', code: 400 };
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id
   * @returns
   */

  async getUserInfoById(id: number): Promise<UserInfo> {
    try {
      return await this.userInfoRepository.findOne({ id });
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data name,address,phone
   * @param token token send by client
   * @returns messages and code
   */

  async updateUserInfo(
    data: UserInfoDto,
    token: string,
  ): Promise<ResponseCommon> {
    try {
      const { address, name, phone, id } = data;
      const id_user = await this.userServices.getIdUserByToken(token);
      const userInfoById = await this.userInfoRepository.findOne({ id });
      if (id_user === userInfoById.id_user) {
        const dataUpdate = { address, name, phone };
        await this.userInfoRepository.update(id, dataUpdate);
        return {
          messages: 'Update user infomation success!',
          code: 200,
        };
      }
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id user info
   * @param token token send with header
   * @returns
   */

  async deleteUserInfo(id: number, token: string): Promise<ResponseCommon> {
    try {
      const id_user = await this.userServices.getIdUserByToken(token);
      const userInfoById = await this.userInfoRepository.findOne({ id });
      if (id_user === userInfoById.id_user) {
        await this.userInfoRepository.delete(id);
        return {
          messages: 'Delete user infomation success!',
          code: 200,
        };
      }
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    return;
  }
}

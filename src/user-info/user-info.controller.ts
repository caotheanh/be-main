import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Headers,
  Param,
  Post,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { UserInfoDto } from 'src/dto/user-info.dto';
import { UserInfoService } from './user-info.service';

@Controller('user-info')
export class UserInfoController {
  constructor(private readonly userInfoServices: UserInfoService) {}

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  get(@Headers() data: any) {
    const { token } = data;
    return this.userInfoServices.getUserInfo(token);
  }

  @Post()
  add(@Body() data: UserInfoDto, @Headers() headers: any) {
    const { token } = headers;
    return this.userInfoServices.addUserInfo(data, token);
  }

  @Get(':id')
  getById(@Param() params: any) {
    const { id } = params;
    return this.userInfoServices.getUserInfoById(id);
  }

  @Put()
  update(@Body() data: UserInfoDto, @Headers() headers: any) {
    const { token } = headers;
    return this.userInfoServices.updateUserInfo(data, token);
  }

  @Delete()
  delete(@Body() id: number, @Headers() headers: any) {
    const { token } = headers;
    return this.userInfoServices.deleteUserInfo(id, token);
  }
}

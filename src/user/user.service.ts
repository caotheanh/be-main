import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { UserDto } from 'src/dto/user.dto';
import { ResponseCommon } from 'src/response/responseCommon';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}
  private TOKEN_HASH_METHOD = 'HS256';

  /**
   * @param password request pasword user
   * @returns password hash
   */

  async hashPassword(password: string): Promise<string> {
    const sailRounds = 10;
    const hashPassword = await bcrypt.hash(password, sailRounds);
    return hashPassword;
  }

  /**
   *
   * @param messages messages send client
   * @param code  code send client
   * @returns password after hash
   */

  sendMessagesAndStatus(messages: string, code: number) {
    return {
      messages,
      code,
    };
  }

  /**
   *
   * @param data headers
   * @returns id user
   */

  async getIdUserByToken(token: string): Promise<number> {
    try {
      const decode = await jwt.verify(token, this.TOKEN_HASH_METHOD);
      const { email } = decode;
      const isUser = await this.findOneUserByEmail(email);
      return isUser.id;
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data email,password
   * @returns messages and code to client
   */

  async register(data: UserDto): Promise<ResponseCommon> {
    try {
      const { email, password } = data;
      const isUser = await this.findOneUserByEmail(email);
      if (!isUser) {
        const role = 1;
        const newPassword = await this.hashPassword(String(password));
        const newDataInsert = {
          email,
          role,
          password: newPassword,
        };
        const insertData = await this.userRepository.create(newDataInsert);
        const result = await this.userRepository.save(insertData);
        return result
          ? this.sendMessagesAndStatus('Create user success!', 200)
          : this.sendMessagesAndStatus('Create user failed!', 400);
      } else {
        return this.sendMessagesAndStatus('Email already exists!', 400);
      }
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param email email user
   * @returns detail user
   */

  async findOneUserByEmail(email: string): Promise<User> {
    try {
      return await this.userRepository.findOne({
        where: {
          email,
        },
      });
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data email and password
   * @returns messages token and code
   */

  async login(data: UserDto): Promise<ResponseCommon> {
    try {
      const { email, password } = data;
      const isUser = await this.findOneUserByEmail(email);
      const passwordMathched = isUser
        ? !!bcrypt.compareSync(String(password), isUser.password)
        : false;
      if (passwordMathched) {
        const expiresIn = { expiresIn: '24h' };
        const payload = { email: isUser.email, role: isUser.role };
        const token = jwt.sign(payload, this.TOKEN_HASH_METHOD, expiresIn);
        return {
          messages: 'Login success!',
          code: 200,
          token,
        };
      } else {
        return {
          messages: 'Please check email or password!',
          code: 400,
        };
      }
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }
}

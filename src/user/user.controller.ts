import { Body, Controller, Post } from '@nestjs/common';
import { UserDto } from 'src/dto/user.dto';
import { ResponseCommon } from 'src/response/responseCommon';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  insertUser(@Body() data: UserDto): Promise<ResponseCommon> {
    return this.userService.register(data);
  }

  @Post('/login')
  loginUser(@Body() data: UserDto): Promise<ResponseCommon> {
    return this.userService.login(data);
  }
}

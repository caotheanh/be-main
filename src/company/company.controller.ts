import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CompanyDto } from 'src/dto/company.dto';
import { CompanyService } from './company.service';

@Controller('company')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @Get()
  get() {
    return this.companyService.listCompany();
  }

  @Get('id')
  getById(@Param() params: any) {
    return this.companyService.listCompanyById(params.id);
  }

  @Post()
  create(@Body() data: CompanyDto) {
    return this.companyService.createCompany(data);
  }

  @Put()
  update(@Param() params: any, @Body() data: CompanyDto) {
    return this.companyService.updateCompany(params.id, data);
  }

  @Delete()
  delete(@Param() params: any) {
    return this.companyService.deleteCompany(params.id);
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCommon } from 'src/response/responseCommon';
import { CompanyDto } from 'src/dto/company.dto';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Company } from './company.entity';

@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
  ) {}

  sendResponse(messages: string, code: number) {
    return {
      messages,
      code,
    };
  }

  /**
   * @returns total and company
   */

  async listCompany(): Promise<[Company[], number]> {
    try {
      return await this.companyRepository.findAndCount();
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id company
   * @returns
   */

  async listCompanyById(id: number): Promise<Company> {
    try {
      return await this.companyRepository.findOne({ id });
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data data insert
   * @returns code and messages
   */

  async createCompany(data: CompanyDto): Promise<ResponseCommon> {
    try {
      const insertData = await this.companyRepository.create(data);
      const result = await this.companyRepository.save(insertData);
      return result
        ? this.sendResponse('Create company success!', 200)
        : this.sendResponse('Create company failed!', 400);
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }
  /**
   *
   * @param data data Update
   * @param id id
   * @returns
   */

  async updateCompany(id: number, data: CompanyDto): Promise<UpdateResult> {
    try {
      return await this.companyRepository.update(id, data);
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id company
   * @returns
   */

  async deleteCompany(id: number): Promise<DeleteResult> {
    try {
      return await this.companyRepository.delete(id);
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }
}

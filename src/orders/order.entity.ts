import { Company } from 'src/company/company.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Orders {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  id_product: string;

  @Column()
  id_user: number;

  @Column()
  address: string;

  @Column()
  phone: string;

  @Column()
  total_number: number;

  @Column()
  price_ship: number;

  @Column()
  total_price: number;

  @Column({ default: 1 })
  status: number;

  @Column()
  payment_type: number;

  @Column()
  company_id: number;

  @Column('text')
  note: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => Company, (company) => company.id)
  @JoinColumn({ name: 'company_id' })
  name: Company;
}

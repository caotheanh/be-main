import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrdersDto } from 'src/dto/order.dto';
import { UserService } from 'src/user/user.service';
import { Repository, UpdateResult } from 'typeorm';
import { Orders } from './order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Orders)
    private readonly oderRepository: Repository<Orders>,
    private readonly userService: UserService,
  ) {}

  /**
   *
   * @param token token send by client headers
   * @returns
   */

  async getOrderByToken(token: string): Promise<Orders[]> {
    try {
      const id_user = this.userService.getIdUserByToken(token);
      return this.oderRepository.find({
        where: {
          id_user,
        },
      });
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param data
   * @returns
   */

  async odersProducts(data: OrdersDto): Promise<Orders> {
    try {
      const result = await this.oderRepository.create(data);
      return await this.oderRepository.save(result);
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }

  /**
   *
   * @param id id order
   * @param status status cancel , pendding, success
   * @returns
   */

  async changeStatus(id: number, status: number): Promise<UpdateResult> {
    try {
      return await this.oderRepository.update(id, { status });
    } catch (error) {
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    }
  }
}
